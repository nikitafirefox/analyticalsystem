from enum import Enum


class TaskType(Enum):
    number = 'Число'
    boolean = 'Да-Нет'
    variable = 'Вариативный'


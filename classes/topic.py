class Topic:

    def __init__(self, name):
        self.name = name
        self.paragraphs = []

    def __str__(self):
        return self.name

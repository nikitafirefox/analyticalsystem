from enums.task import TaskType


class Task:
    def __init__(self, number, desc, answer, answer_type=TaskType.number, variations=[]):
        self.number = number
        self.desc = desc
        self.answer = answer
        self.type = answer_type
        self.variations = variations

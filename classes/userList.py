import pickle
import os


class UserList:
    def __init__(self):
        self.users = []

    def serialize(self, path):
        with open(path + '.pickle', 'wb') as f:
            pickle.dump(self.users, f)

    def deserialize(self, path):
        if os.path.isfile(path + '.pickle') and os.path.getsize(path + '.pickle') > 0:
            with open(path + '.pickle', 'rb') as f:
                self.users = pickle.load(f)

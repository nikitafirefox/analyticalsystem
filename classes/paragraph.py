class Paragraph:

    def __init__(self, name, next_topics=[], max_task_count=5):
        self.name = name
        self.tasks = []
        self.next_topics = next_topics
        self.max_task_count = max_task_count

import pickle
import os
from pyvis import network as net
import csv

from classes.task import Task
from classes.paragraph import Paragraph
from classes.topic import Topic
from enums.task import TaskType


class LearningData:

    def __init__(self):
        self.topics = []

    def serialize(self, path):
        with open(path + '.pickle', 'wb') as f:
            pickle.dump(self.topics, f)

    def deserialize(self, path):
        if os.path.isfile(path + '.pickle') and os.path.getsize(path + '.pickle') > 0:
            with open(path + '.pickle', 'rb') as f:
                self.topics = pickle.load(f)

    def save_graph(self, path):
        g = net.Network(height='100%', width='100%')
        for t in self.topics:
            g.add_node(t.name, physics=False)

        for t in self.topics:
            for p in t.paragraphs:
                for nt in p.next_topics:
                    edge = next((x for x in g.get_edges()
                                if x['to'] == nt.name
                                and x['from'] == t.name), None)

                    if edge is not None:
                        edge['title'] += '<li>' + p.name + '</li>'
                    else:
                        g.add_edge(t.name, nt.name,
                                   title='<div style="padding: 4px;"><b>Связанные параграфы</b>' +
                                         '<ul style="margin: 0px; padding: 4px 20px;"><li>' +
                                         p.name + '</li>', physics=False)

        for edge in g.get_edges():
            edge['title'] += '</ul></div>'
        g.save_graph(path + '.html')

    def export_csv(self, path):
        data = []
        for t in self.topics:
            data.append(['Г', t.name])
            for p in t.paragraphs:
                next_topics = ''
                for nt in p.next_topics:
                    next_topics += nt.name + ' | '
                next_topics = next_topics.strip()
                data.append(['', 'П', p.name, next_topics, str(p.max_task_count)])
                for ts in p.tasks:
                    data.append(['', '', 'З', ts.number, ts.desc, ts.answer, ts.type.value, ' | '.join(ts.variations)])

        file = open(path, 'w', newline='')
        with file:
            writer = csv.writer(file, delimiter=";")
            writer.writerows(data)

    def import_csv(self, path):

        rows = []
        with open(path) as File:
            reader = csv.reader(File, delimiter=';')

            for row in reader:
                rows.append(row)

        if len(rows) < 1:
            return

        self.topics.clear()

        for row in rows:
            if len(row) > 1 and row[0].lower().strip() == 'г' and row[1].strip() != '':
                self.topics.append(Topic(row[1].strip()))

        topic = None
        paragraph = None
        for row in rows:
            if len(row) > 0 and row[0].lower().strip() == 'г':
                if len(row) > 1 and row[1].strip() != '':
                    topic = next((x for x in self.topics if x.name == row[1].strip()), None)
                else:
                    topic = None
                paragraph = None

            if topic is None:
                continue

            if len(row) > 1 and row[0].strip() == '' and\
                    (row[1].lower().strip() == 'п' or row[1].lower().strip() == 'т'):
                if len(row) > 2 and row[2].strip() != '':
                    paragraph = Paragraph(row[2].strip())

                    if len(row) > 4 and row[4].isnumeric():
                        paragraph.max_task_count = int(row[4])

                    if len(row) > 3 and row[3].strip() != '':
                        nt_names = row[3].strip().split('|')

                        next_topics = []
                        for nt in nt_names:

                            if nt == '':
                                continue

                            next_topic = next((x for x in self.topics if x.name == nt.strip()), None)

                            if next_topic is not None:
                                next_topics.append(next_topic)

                        paragraph.next_topics = next_topics

                    topic.paragraphs.append(paragraph)
                else:
                    paragraph = None

            if paragraph is None:
                continue

            if len(row) > 2 and row[0].strip() == '' and row[1].strip() == '' and row[2].lower().strip() == 'з':
                if len(row) > 5 and row[3].strip() != '' and row[4].strip() != '' and row[5].strip() != '':
                    number = row[3].strip()
                    desc = row[4].strip()
                    answer = row[5].strip()
                    if len(row) == 6 and answer.isnumeric():
                        paragraph.tasks.append(Task(number, desc, answer))
                    elif len(row) > 6 and row[6].strip() != '':
                        task_type = row[6].strip()

                        if task_type == TaskType.number.value and answer.isnumeric():
                            paragraph.tasks.append(Task(number, desc, answer))

                        elif task_type == TaskType.boolean.value and (answer == 'Да' or answer == 'Нет'):
                            paragraph.tasks.append(Task(number, desc, answer, TaskType.boolean))

                        elif task_type == TaskType.variable.value and len(row) > 7 and row[7].strip() != '':
                            variations = row[7].strip().split('|')
                            for i in range(len(variations)):
                                variations[i] = variations[i].strip()
                            unique_variations = True
                            for i in range(len(variations)):
                                for j in range(len(variations)):
                                    if (i != j and variations[i] == variations[j]) \
                                            or variations[i] == answer:
                                        unique_variations = False
                                        break
                                if not unique_variations:
                                    break

                            if unique_variations:
                                paragraph.tasks.append(Task(number, desc, answer, TaskType.variable, variations))


from pyvis import network as net
import math
# k1 = 1.5
# k2 = 0.1

class Dot:
    def __init__(self, name):
        self.name = name
        self.count = 1

class User:
    def __init__(self, first_name, second_name='', third_name='', recommend=''):
        self.first_name = first_name
        self.second_name = second_name
        self.third_name = third_name
        self.topics = []
        self.recommend = recommend

    def __str__(self):
        s = self.first_name + ' ' + self.second_name + ' ' + self.third_name + '\n'
        for t in self.topics:
            s += '- ' + t.name + '\n'
            for p in t.paragraphs:
                s += '-- ' + p.name + ' ' + str(p.success_task_count) + '/' + str(p.task_count) + '\n'

        return s

    def save_graph(self, path):
        g = net.Network(height='100%', width='100%')
        for t in self.topics:
            g.add_node(t.name, physics=False)

        edges = []
        for t in self.topics:
            for p in t.paragraphs:
                for nt in p.next_topics:
                    edge = next((x for x in edges
                                 if x.n1 == t.name
                                 and x.n2 == nt.name), None)

                    if edge is not None:
                        edge.title += '<li>' + p.name + ' ' + str(p.success_task_count) + \
                                      '/' + str(p.task_count) + '</li>'
                    else:
                        edge = Edge(t.name, nt.name, '<b>Связанные параграфы</b>' +
                                    '<ul style="margin: 0px; padding: 4px 20px;"><li>' + p.name + ' ' +
                                    str(p.success_task_count) + '/' + str(p.task_count) + '</li>')
                        edges.append(edge)
                    edge.paragraphs.append(p)


        arrayDot = []
        for edge in edges:
            edge.title += '</ul></div>'
            res = edge.network()
            g.add_edge(edge.n1, edge.n2,
                       title='<div style="padding: 4px;">' + to_percent(res) + '<br/>' + edge.title,
                       physics=False,
                       color=get_color(res))

            if res <= 0.25:

                dot = next((x for x in arrayDot if x.name == edge.n1), None)
                if dot is not None:
                    dot.count = dot.count + 1
                else:
                    arrayDot.append(Dot(edge.n1))

                dot = next((x for x in arrayDot if x.name == edge.n2), None)
                if dot is not None:
                    dot.count = dot.count + 1
                else:
                    arrayDot.append(Dot(edge.n2))

        arrayDot.sort(key=lambda x: x.name, reverse=True)
        if len(arrayDot) > 0:
            maxDot = arrayDot[0]
            for d in arrayDot:
                if maxDot.count < d.count:
                    maxDot = d

            self.recommend = maxDot.name
        else:
            self.recommend = ''

        g.save_graph(path + '.html')




class Edge:
    def __init__(self, n1, n2, title):
        self.n1 = n1
        self.n2 = n2
        self.title = title
        self.paragraphs = []

    def network(self):
        total_task_count = 0
        total_success_count = 0
        total_success_paragraph_count = 0
        for p in self.paragraphs:
            total_task_count += p.task_count
            total_success_count += p.success_task_count
            if p.success_task_count == p.task_count:
                total_success_paragraph_count += 1

        h1 = 0
        h2 = 0
        for p in self.paragraphs:
            h1 += p.success_task_count * p.task_count / total_task_count
            h2 += p.success_task_count * total_success_count / total_task_count

        # k1 = total_success_paragraph_count / len(self.paragraphs)
        k1 = 1 / (total_success_count * 1.5) if total_success_count > 0 else 0
        k2 = total_success_count / total_task_count if total_success_count < total_task_count else 3
        res = fun(fun(h1)*k1 + fun(h2)*k2)
        return res


def get_color(n):
    if n <= 0.25:
        return 'red'
    elif n <= 0.5:
        return 'orange'
    elif n <= 0.8:
        return 'yellow'
    elif n > 1:
        return 'black'
    else:
        return 'green'


def fun(x):
    return (math.exp(2 * x) - 1) / (1 + math.exp(2 * x))
    # return 1 / (1 + math.exp(-x))


def to_percent(x):
    return str(int(float('{:.2f}'.format(x)) * 100)) + '%'

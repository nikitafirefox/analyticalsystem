from bots.bot import Bot, BotTopic
from classes.testParagraphs import TestParagraphs
from classes.topic import Topic
from classes.user import User
import random


def bot_startup(topics, count=5):
    if 0 < count > 100:
        count = 5

    users = []
    for i in range(count):
        b = Bot('Bot ' + str(i))
        u = User(b.name)

        for t in topics:
            b.topics.append(BotTopic(t.name))
            u.topics.append(Topic(t.name))

        for j in range(len(topics)):
            for p in topics[j].paragraphs:

                max_count = p.max_task_count if len(p.tasks) > p.max_task_count else len(p.tasks)
                if max_count < 1:
                    continue
                success = 0
                for g in range(max_count):
                    r = random.uniform(0, 1)
                    if r > 1 - b.topics[j].error_probability:
                        success += 1

                next_topics = []
                for nt in p.next_topics:
                    topic = next((x for x in b.topics if x.name == nt.name), None)
                    u_topic = next((x for x in u.topics if x.name == nt.name), None)

                    if topic is not None:
                        topic.addProbability(b.topics[j].error_probability)

                    if u_topic is not None:
                        next_topics.append(u_topic)

                u.topics[j].paragraphs.append(TestParagraphs(p.name, next_topics, max_count, success))

        users.append(u)

    return users

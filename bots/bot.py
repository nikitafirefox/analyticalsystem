from classes.topic import Topic
import random


class Bot:
    def __init__(self, name):
        self.name = name
        self.topics = []


class BotTopic(Topic):
    def __init__(self, name, error_probability=-1):
        super().__init__(name)
        if 0 <= error_probability <= 1:
            self.error_probability = (random.uniform(0, 1) + error_probability) / 2
        else:
            self.error_probability = random.uniform(0, 1)

    def addProbability(self, error_probability):
        if 0 <= error_probability <= 1:
            self.error_probability = (self.error_probability + error_probability) / 2

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Nikita\Desktop\analyticalSystem\views\topicSettings.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(280, 90)
        Form.setMinimumSize(QtCore.QSize(280, 90))
        Form.setMaximumSize(QtCore.QSize(280, 90))
        font = QtGui.QFont()
        font.setPointSize(8)
        Form.setFont(font)
        self.nameEdit = QtWidgets.QLineEdit(Form)
        self.nameEdit.setGeometry(QtCore.QRect(100, 10, 171, 21))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.nameEdit.setFont(font)
        self.nameEdit.setObjectName("nameEdit")
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(10, 10, 91, 20))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.saveBtn = QtWidgets.QPushButton(Form)
        self.saveBtn.setEnabled(False)
        self.saveBtn.setGeometry(QtCore.QRect(80, 50, 131, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.saveBtn.setFont(font)
        self.saveBtn.setObjectName("saveBtn")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Настройка главы"))
        self.label.setText(_translate("Form", "Название"))
        self.saveBtn.setText(_translate("Form", "Сохранить"))

from classes.learning import LearningData
from classes.testParagraphs import TestParagraphs
from classes.topic import Topic
from classes.user import User
from classes.userList import UserList
from enums.task import TaskType
from layers.numberTaskDlg import NumberTaskLayout
from layers.topics import TopicsLayer
from layers.trueFalseTaskDlg import TrueFalseTaskLayout
from layers.users import UsersLayout
from layers.countBots import CountBotsLayout
from layers.userReg import UserRegLayout
from layers.variableTaskDlg import VariableTaskLayout
from views.app import Ui_Form
from bots.botStartup import bot_startup
from PyQt5 import QtWidgets
import random
import numpy as np
from pyvis import network as net
from layers.graph import Graph

path_learnings = 'learnings'
path_users = 'users'


class AppLayer(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.learnings = LearningData()
        self.user_list = UserList()
        self.user_list.deserialize(path_users)
        self.learnings.deserialize(path_learnings)
        self.ui.topicsBtn.clicked.connect(self.topics_btn_click)
        self.ui.usersBtn.clicked.connect(self.users_btn_click)
        self.set_enable()
        self.ui.startBotsBtn.clicked.connect(self.start_bots)
        self.ui.startTestBtn.clicked.connect(self.start_test)
        self.ui.networkBtn.clicked.connect(self.networkBtnClick)

    def closeEvent(self, event):
        self.learnings.serialize(path_learnings)
        self.user_list.serialize(path_users)

    def set_enable(self):
        self.ui.startBotsBtn.setEnabled(len(self.learnings.topics) > 0)
        self.ui.startTestBtn.setEnabled(len(self.learnings.topics) > 0)
        self.ui.networkBtn.setEnabled(len(self.user_list.users) > 0)

    def topics_btn_click(self):
        dlg_topics = TopicsLayer(self, self.learnings)
        self.hide()
        dlg_topics.show()
        self.set_enable()

    def users_btn_click(self):
        dlg_users = UsersLayout(self, self.user_list.users)
        self.hide()
        dlg_users.show()

    def start_bots(self):
        dlg_bots = CountBotsLayout(self)
        self.hide()
        dlg_bots.exec()
        if dlg_bots.dlg_result:
            user_list = bot_startup(self.learnings.topics, dlg_bots.count)
            for u in user_list:
                old_user = next((x for x in self.user_list.users if x.first_name == u.first_name and
                                 x.second_name == u.second_name and
                                 x.third_name == u.third_name), None)
                if old_user is not None:
                    old_user.topics = u.topics
                else:
                    self.user_list.users.append(u)
        self.set_enable()

    def start_test(self):
        dlg_user = UserRegLayout(self)
        self.hide()
        dlg_user.exec()
        if dlg_user.dlgRes:

            user = next((x for x in self.user_list.users if x.first_name == dlg_user.fName and
                         x.second_name == dlg_user.sName and
                         x.third_name == dlg_user.tName), None)

            if user is None:
                user = User(dlg_user.fName, dlg_user.sName, dlg_user.tName)

            all_tasks = 0
            for t in self.learnings.topics:
                user.topics.append(Topic(t.name))
                for p in t.paragraphs:
                    all_tasks += len(p.tasks)

            current_task = 0
            for i in range(len(user.topics)):

                cancel = False
                for p in self.learnings.topics[i].paragraphs:
                    max_count = p.max_task_count if len(p.tasks) > p.max_task_count else len(p.tasks)

                    if max_count < 1:
                        continue

                    success = 0

                    tasks = p.tasks[:]

                    random.shuffle(tasks)

                    for j in range(max_count):
                        current_task += 1
                        task_dlg = None
                        if tasks[j].type == TaskType.number:
                            task_dlg = NumberTaskLayout(self, user.topics[i].name, p.name,
                                                        str(current_task) + '/' + str(all_tasks), tasks[j].desc)
                        elif tasks[j].type == TaskType.boolean:
                            task_dlg = TrueFalseTaskLayout(self, user.topics[i].name, p.name,
                                                           str(current_task) + '/' + str(all_tasks), tasks[j].desc)
                        elif tasks[j].type == TaskType.variable and len(tasks[j].variations):
                            variations = tasks[j].variations[:]
                            variations.append(tasks[j].answer)
                            random.shuffle(variations)

                            task_dlg = VariableTaskLayout(self, user.topics[i].name, p.name,
                                                          str(current_task) + '/' + str(all_tasks), tasks[j].desc,
                                                          variations)

                        if task_dlg is not None:
                            task_dlg.exec()

                            if task_dlg.cancel_test:
                                cancel = True
                                break

                            if task_dlg.answer == str(tasks[j].answer):
                                success += 1

                    if cancel:
                        break

                    next_topics = []
                    for nt in p.next_topics:
                        topic = next((x for x in user.topics if x.name == nt.name), None)
                        if topic is not None:
                            next_topics.append(topic)

                    user.topics[i].paragraphs.append(TestParagraphs(p.name, next_topics, max_count, success))

                if cancel:
                    break
            self.user_list.users.append(user)

    def networkBtnClick(self):
        A = np.arange(len(self.learnings.topics), dtype=float)
        B = np.arange(len(self.learnings.topics), dtype=float)
        w = None
        for u in self.user_list.users:

            for i in range(len(u.topics)):
                total_task_count = 0
                total_success_count = 0
                for p in u.topics[i].paragraphs:
                    total_task_count += p.task_count
                    total_success_count += p.success_task_count

                A[i] = total_success_count / total_task_count

            if w is None:
                w = np.zeros((len(self.learnings.topics), len(self.learnings.topics)))

                for ii in range(len(self.learnings.topics)):
                    for jj in range(len(self.learnings.topics)):
                        if ii == jj:
                            w[ii][jj] = 0
                        else:
                            w[ii][jj] =(A[ii] - A[jj]) if (A[ii] - A[jj]) < 0 else 1 - (A[ii] - A[jj])

            B = fun(np.dot(A, w))

            delta = (A - B) * (1 - (B**2))

            w += 0.001 * (A.T.dot(delta))

        #print(w)
        # отрисовка
        g = net.Network(height='100%', width='100%')
        for t in self.learnings.topics:
            g.add_node(t.name, physics=False)

        for i in range(len(self.learnings.topics)):
            for j in range(len(self.learnings.topics)):
                if j > i and w[i][j] >= 0.5:
                    g.add_edge(self.learnings.topics[i].name, self.learnings.topics[j].name, physics=False)

        g.save_graph('network.html')
        graph_dlg = Graph(self, 'network.html')
        graph_dlg.setWindowTitle('Граф связей')
        graph_dlg.showMaximized()


def fun(x):
    return (np.exp(2 * x) - 1) / (1 + np.exp(2 * x))

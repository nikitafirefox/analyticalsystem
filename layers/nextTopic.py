from PyQt5 import QtWidgets
from views.nextTopic import Ui_Form


class NextTopicLayout(QtWidgets.QDialog):
    def __init__(self, parent=None, topics=[], next_topics=[]):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.topics = topics
        self.next_topics = next_topics

        for t in topics:
            self.ui.nextTopicComboBox.addItem(t.name)

        self.ui.saveBtn.setEnabled(len(topics) > 0)

        self.ui.saveBtn.clicked.connect(self.save)

    def closeEvent(self, event):
        self.parent().show()

    def save(self):
        self.next_topics.append(self.topics[self.ui.nextTopicComboBox.currentIndex()])
        self.close()

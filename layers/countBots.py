from views.countBots import Ui_Form
from PyQt5 import QtWidgets


class CountBotsLayout(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.count = 5
        self.ui.countEdit.textChanged.connect(self.start_count_enabled)
        self.ui.countEdit.setText(str(self.count))
        self.dlg_result = False
        self.ui.startBtn.clicked.connect(self.start)

    def start_count_enabled(self):
        self.ui.startBtn.setEnabled(self.ui.countEdit.text().strip().isnumeric() and
                                    0 < int(self.ui.countEdit.text().strip()) < 101)

    def start(self):
        self.dlg_result = True
        self.count = int(self.ui.countEdit.text().strip())
        self.close()

    def closeEvent(self, event):
        self.parent().show()

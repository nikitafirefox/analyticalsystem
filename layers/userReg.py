from PyQt5 import QtWidgets
from views.userReg import Ui_Form


class UserRegLayout(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.ui.fEdit.textChanged.connect(self.change)
        self.ui.sEdit.textChanged.connect(self.change)
        self.sName = ''
        self.fName = ''
        self.tName = ''
        self.dlgRes = False
        self.ui.startBtn.clicked.connect(self.start)

    def change(self):
        self.ui.startBtn.setEnabled(self.ui.sEdit.text().strip() != '' and self.ui.fEdit.text().strip() != '')

    def closeEvent(self, event):
        self.parent().show()

    def start(self):
        self.sName = self.ui.sEdit.text().strip()
        self.fName = self.ui.fEdit.text().strip()
        self.tName = self.ui.tEdit.text().strip()
        self.dlgRes = True
        self.close()

from PyQt5 import QtWidgets
from views.taskSettings import Ui_Form
from classes.task import Task
from enums.task import TaskType


class TaskSettingsLayer(QtWidgets.QDialog):
    def __init__(self, parent=None, tasks=[], task=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.tasks = tasks
        self.task = task

        self.task_types = list(TaskType)
        for i in range(len(self.task_types)):
            self.ui.typeComboBox.addItem(self.task_types[i].value)
            if task is not None and task.type == self.task_types[i]:
                self.ui.typeComboBox.setCurrentIndex(i)

        self.type_changed()

        if task is not None:
            self.ui.numberEdit.setText(task.number)
            self.ui.answerEdit.setText(str(task.answer))
            self.ui.descEdit.insertPlainText(task.desc)
            self.ui.variantsEdit.setText(' | '.join(task.variations))

        self.ui.typeComboBox.currentIndexChanged.connect(self.type_changed)
        self.ui.numberEdit.textChanged.connect(self.change)
        self.ui.answerEdit.textChanged.connect(self.change)
        self.ui.variantsEdit.textChanged.connect(self.change)
        self.ui.descEdit.textChanged.connect(self.change)
        self.change()

        self.ui.saveBtn.clicked.connect(self.save)

    def change(self):
        number = self.ui.numberEdit.text().strip()
        unique_number = True
        for t in self.tasks:
            if self.task is not None and number == self.task.number:
                continue
            elif t.number == number:
                unique_number = False
                break
        b = number != '' and unique_number and self.ui.descEdit.toPlainText().strip() != ''
        if b:
            i = self.ui.typeComboBox.currentIndex()
            if self.task_types[i] == TaskType.boolean:
                b = self.ui.answerEdit.text().strip() == 'Да' or self.ui.answerEdit.text().strip() == 'Нет'
            elif self.task_types[i] == TaskType.number:
                b = self.ui.answerEdit.text().strip().isnumeric()
            elif self.task_types[i] == TaskType.variable:
                variations = self.ui.variantsEdit.text().strip().split('|')
                unique_variations = True
                for i in range(len(variations)):
                    for j in range(len(variations)):
                        if (i != j and variations[i].strip() == variations[j].strip())\
                                or variations[i].strip() == self.ui.answerEdit.text().strip():
                            unique_variations = False
                            break
                    if not unique_variations:
                        break
                b = self.ui.answerEdit.text().strip() != ''\
                    and self.ui.variantsEdit.text().strip() != ''\
                    and unique_variations
        self.ui.saveBtn.setEnabled(b)

    def type_changed(self):
        self.ui.variantsEdit.setText('')
        self.ui.variantsEdit.setEnabled(self.task_types[self.ui.typeComboBox.currentIndex()] == TaskType.variable)
        self.change()

    def save(self):
        number = self.ui.numberEdit.text().strip()
        desc = self.ui.descEdit.toPlainText().strip()
        task_type = self.task_types[self.ui.typeComboBox.currentIndex()]
        answer = self.ui.answerEdit.text().strip()
        variations = []
        if task_type == TaskType.variable:
            variations = self.ui.variantsEdit.text().strip().split('|')
            for i in range(len(variations)):
                variations[i] = variations[i].strip()

        if self.task is not None:
            self.task.number = number
            self.task.desc = desc
            self.task.answer = answer
            self.task.type = task_type
            self.task.variations = variations
        else:
            self.tasks.append(Task(number, desc, answer, task_type, variations))

        self.close()

    def closeEvent(self, event):
        self.parent().show()

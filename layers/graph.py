import os
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5.QtWebEngineWidgets import QWebEngineView


class Graph(QtWidgets.QDialog):
    def __init__(self, parent=None, file=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.ui.browser.load(QtCore.QUrl.fromLocalFile(os.path.abspath(file)))

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        self.browser = QWebEngineView(Form)
        Form.resize(229, 93)
        Form.setMinimumSize(QtCore.QSize(800, 600))
        Form.setMaximumSize(QtCore.QSize(800, 600))
        self.browser.setGeometry(QtCore.QRect(0, 0, 800, 600))
        self.browser.setObjectName("browser")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Граф"))

from PyQt5 import QtWidgets
from views.topicSettings import Ui_Form
from classes.topic import Topic


class TopicSettingsLayer(QtWidgets.QDialog):
    def __init__(self, parent=None, topics=[], topic=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.topics = topics
        self.topic = topic
        if topic is not None:
            self.ui.nameEdit.setText(topic.name)
            self.changed()
        self.ui.saveBtn.clicked.connect(self.save)
        self.ui.nameEdit.textChanged.connect(self.changed)

    def save(self):
        if self.topic is not None:
            self.topic.name = self.ui.nameEdit.text().strip()
        else:
            self.topics.append(Topic(self.ui.nameEdit.text().strip()))
        self.close()

    def changed(self):
        name = self.ui.nameEdit.text().strip()
        unique_name = True
        for t in self.topics:
            if self.topic is not None and name == self.topic.name:
                continue
            elif t.name == name:
                unique_name = False
                break
        self.ui.saveBtn.setEnabled(name != '' and unique_name)

    def closeEvent(self, event):
        self.parent().show()

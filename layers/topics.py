import views.listForm as listForm
from PyQt5 import QtWidgets
from layers.graph import Graph
from layers.paragraphs import ParagraphsLayer
from layers.topicSettings import TopicSettingsLayer


class TopicsLayer(QtWidgets.QDialog):
    def __init__(self, parent=None, learnings=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = listForm.Ui_Form()
        self.ui.setupUi(self)
        self.setWindowTitle('Список глав')
        self.ui.extraBtn1.setEnabled(False)
        self.ui.extraBtn1.setText('Граф')
        self.ui.extraBtn2.setEnabled(True)
        self.ui.extraBtn2.setText('Импорт')
        self.ui.extraBtn3.setEnabled(False)
        self.ui.extraBtn3.setText('Экспорт')
        self.learnings = learnings
        self.topics = learnings.topics
        self.ui.items.doubleClicked.connect(self.list_double_clicked)
        self.ui.extraBtn1.clicked.connect(self.show_graph)
        self.ui.extraBtn2.clicked.connect(self.open_csv)
        self.ui.extraBtn3.clicked.connect(self.save_csv)
        self.ui.items.currentRowChanged.connect(self.selected_row_changed)
        self.ui.deleteBtn.clicked.connect(self.delete_row)
        self.ui.addBtn.clicked.connect(self.add_topic)
        self.ui.editBtn.clicked.connect(self.edit_topic)
        self.selected_row = None

    def closeEvent(self, event):
        self.parent().show()

    def showEvent(self, event):
        self.selected_row_changed(None)
        self.set_list()

    def set_list(self):
        self.ui.items.clear()
        for t in self.topics:
            self.ui.items.addItem(t.name)
        if len(self.topics) > 0:
            self.ui.extraBtn1.setEnabled(True)
            self.ui.extraBtn3.setEnabled(True)
        else:
            self.ui.extraBtn1.setEnabled(False)
            self.ui.extraBtn3.setEnabled(False)

    def list_double_clicked(self, event):
        dlg = ParagraphsLayer(self, paragraphs=self.topics[event.row()].paragraphs, topics=self.topics,
                              topic=self.topics[event.row()])
        self.hide()
        dlg.show()

    def show_graph(self):
        self.learnings.save_graph('graph')
        graph_dlg = Graph(self, 'graph.html')
        graph_dlg.showMaximized()

    def selected_row_changed(self, index):
        self.selected_row = index
        if self.selected_row is not None and self.selected_row >= 0:
            self.ui.deleteBtn.setEnabled(True)
            self.ui.editBtn.setEnabled(True)
        else:
            self.ui.deleteBtn.setEnabled(False)
            self.ui.editBtn.setEnabled(False)

    def delete_row(self):
        if self.selected_row is not None:
            self.topics.pop(self.selected_row)
            self.set_list()

    def edit_topic(self):
        if self.selected_row is not None:
            settings = TopicSettingsLayer(self, topics=self.topics, topic=self.topics[self.selected_row])
            self.hide()
            settings.show()

    def add_topic(self):
        settings = TopicSettingsLayer(self, topics=self.topics)
        self.hide()
        settings.show()

    def save_csv(self):
        file_name = QtWidgets.QFileDialog.getSaveFileName(self, 'Save', '/', 'CSV (*.csv)')[0]
        if file_name:
            self.learnings.export_csv(file_name)

    def open_csv(self):
        file_name = QtWidgets.QFileDialog.getOpenFileName(self, 'Open', '/', 'CSV (*.csv)')[0]
        if file_name:
            self.learnings.import_csv(file_name)

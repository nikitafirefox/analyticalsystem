from views.listForm import Ui_Form
from PyQt5 import QtWidgets
from layers.taskSettings import TaskSettingsLayer


class TasksLayer(QtWidgets.QDialog):
    def __init__(self, parent=None, tasks=[], paragraph_name=''):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.setFixedHeight(250)
        self.setWindowTitle(paragraph_name + ': список задач')
        self.ui.extraBtn1.hide()
        self.ui.extraBtn2.hide()
        self.ui.extraBtn3.hide()
        self.tasks = tasks
        self.ui.items.currentRowChanged.connect(self.selected_row_changed)
        self.ui.deleteBtn.clicked.connect(self.delete_task)
        self.ui.addBtn.clicked.connect(self.add_task)
        self.ui.editBtn.clicked.connect(self.edit_task)
        self.selected_row = None

    def closeEvent(self, event):
        self.parent().show()

    def showEvent(self, event):
        self.set_list()

    def set_list(self):
        self.ui.items.clear()
        for t in self.tasks:
            self.ui.items.addItem(t.number)

    def selected_row_changed(self, index):
        self.selected_row = index
        if self.selected_row is not None and self.selected_row >= 0:
            self.ui.deleteBtn.setEnabled(True)
            self.ui.editBtn.setEnabled(True)

    def delete_task(self):
        if self.selected_row is not None:
            self.tasks.pop(self.selected_row)
            self.set_list()
        self.ui.deleteBtn.setEnabled(False)
        self.ui.editBtn.setEnabled(False)

    def edit_task(self):
        if self.selected_row is not None:
            settings = TaskSettingsLayer(self, tasks=self.tasks,
                                                           task=self.tasks[self.selected_row])
            self.hide()
            settings.show()
            self.ui.deleteBtn.setEnabled(False)
            self.ui.editBtn.setEnabled(False)

    def add_task(self):
        settings = TaskSettingsLayer(self, tasks=self.tasks)
        self.hide()
        settings.show()
        self.ui.deleteBtn.setEnabled(False)
        self.ui.editBtn.setEnabled(False)

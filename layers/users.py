from views.users import Ui_Form
from layers.userRes import UserResLayout
from layers.graph import Graph
from PyQt5 import QtWidgets
import numpy as np


class UsersLayout(QtWidgets.QDialog):
    def __init__(self, parent=None, users=[]):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.users = users

        self.ui.deleteBtn.clicked.connect(self.delete)
        self.ui.userList.doubleClicked.connect(self.list_double_clicked)
        self.ui.userList.currentRowChanged.connect(self.selected_row_changed)
        self.ui.graphBtn.clicked.connect(self.show_graph)
        self.ui.clearBtn.clicked.connect(self.clear)
        self.selected_row = None

    def closeEvent(self, event):
        self.parent().show()

    def showEvent(self, event):
        self.selected_row_changed(None)
        self.set_list()

    def set_list(self):
        self.ui.userList.clear()
        for u in self.users:
            self.ui.userList.addItem(str(u.second_name + ' ' + u.first_name + ' ' + u.third_name).strip())
        if len(self.users) > 0:
            self.ui.clearBtn.setEnabled(True)
        else:
            self.ui.clearBtn.setEnabled(False)

    def selected_row_changed(self, index):
        self.selected_row = index
        if self.selected_row is not None and self.selected_row >= 0:
            self.ui.deleteBtn.setEnabled(True)
            self.ui.graphBtn.setEnabled(True)
        else:
            self.ui.deleteBtn.setEnabled(False)
            self.ui.graphBtn.setEnabled(False)

    def delete(self):
        if self.selected_row is not None:
            self.users.pop(self.selected_row)
            self.set_list()

    def list_double_clicked(self, event):
        dlg = UserResLayout(self, user=self.users[event.row()])
        self.hide()
        dlg.show()

    def show_graph(self):
        if self.selected_row is not None:

            # a = np.zeros((len(self.users), len(self.users[self.selected_row].topics)))
            #
            # for i in range(len(self.users)):
            #     for j in range(len(self.users[self.selected_row].topics)):
            #         total = 0
            #         for p in self.users[i].topics[j].paragraphs:
            #             total = total + p.success_task_count
            #         a[i][j] = total
            #
            # resA = np.corrcoef(a.T)

            self.users[self.selected_row].save_graph('user')
            graph_dlg = Graph(self, 'user.html')
            userName = self.users[self.selected_row].first_name

            if self.users[self.selected_row].second_name != '':
                userName = userName + ' ' + self.users[self.selected_row].second_name

            if self.users[self.selected_row].third_name != '':
                userName = userName + ' ' + self.users[self.selected_row].third_name

            name = 'Граф ' + userName
            if self.users[self.selected_row].recommend != '':
                name = name + ' Рекомендация: повторить ' + self.users[self.selected_row].recommend
            graph_dlg.setWindowTitle(name)
            graph_dlg.showMaximized()

    def clear(self):
        self.users.clear()
        self.set_list()

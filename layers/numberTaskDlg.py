from PyQt5 import QtWidgets
from views.numberTaskDlg import Ui_Form


class NumberTaskLayout(QtWidgets.QDialog):
    def __init__(self, parent=None, tName='', pName='',  task_number='', desc=''):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.topicEdit.setText(tName)
        self.ui.paragraphEdit.setText(pName)
        self.ui.numberEdit.setText(task_number)
        self.ui.descEdit.insertPlainText(desc)
        self.cancel_test = False

        self.answer = ''

        self.ui.saveBtn.clicked.connect(self.save)
        self.ui.answerEdit.textChanged.connect(self.change)
        self.ui.cancelBtn.clicked.connect(self.cancel)

    def cancel(self):
        self.cancel_test = True
        self.close()

    def closeEvent(self, event):
        self.parent().show()

    def change(self):
        self.ui.saveBtn.setEnabled(self.ui.answerEdit.text().strip() != '')

    def save(self):
        self.answer = self.ui.answerEdit.text().strip()
        self.close()

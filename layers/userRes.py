from PyQt5 import QtWidgets
from views.userRes import Ui_Form


class UserResLayout(QtWidgets.QDialog):
    def __init__(self, parent=None, user=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        if user is not None:
            self.ui.resEdit.insertPlainText(str(user))

    def closeEvent(self, event):
        self.parent().show()

from PyQt5 import QtWidgets
from views.paragraphSettings import Ui_Form
from layers.nextTopic import NextTopicLayout
from classes.paragraph import Paragraph


class ParagraphSettingsLayout(QtWidgets.QDialog):
    def __init__(self, parent=None, paragraphs=[], topics=[], paragraph=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.paragraphs = paragraphs
        self.paragraph = paragraph
        self.topics = topics
        self.next_topics = []
        self.ui.maxCountEdit.setText('5')
        if paragraph is not None:
            for nt in paragraph.next_topics:
                self.next_topics.append(nt)
            self.ui.nameEdit.setText(paragraph.name)
            self.ui.maxCountEdit.setText(str(paragraph.max_task_count))
            self.changed()
        self.ui.saveBtn.clicked.connect(self.save)
        self.ui.addBtn.clicked.connect(self.add_next_topic)
        self.ui.deleteBtn.clicked.connect(self.delete_next_topic)
        self.ui.nameEdit.textChanged.connect(self.changed)
        self.ui.maxCountEdit.textChanged.connect(self.changed)
        self.ui.listNextTopics.currentRowChanged.connect(self.selected_row_changed)
        self.selected_row = None

    def changed(self):
        name = self.ui.nameEdit.text().strip()
        unique_name = True
        for p in self.paragraphs:
            if self.paragraph is not None and name == self.paragraph.name:
                continue
            elif p.name == name:
                unique_name = False
                break
        self.ui.saveBtn.setEnabled(name != '' and unique_name and
                                   self.ui.maxCountEdit.text().strip().isnumeric() and
                                   0 < int(self.ui.maxCountEdit.text().strip()) < 6 and
                                   len(self.next_topics) > 0)

    def closeEvent(self, event):
        self.parent().show()

    def save(self):
        if self.paragraph is not None:
            self.paragraph.name = self.ui.nameEdit.text().strip()
            self.paragraph.max_task_count = int(self.ui.maxCountEdit.text().strip())
            self.paragraph.next_topics = self.next_topics
        else:
            self.paragraphs.append(Paragraph(self.ui.nameEdit.text().strip(),
                                             self.next_topics, int(self.ui.maxCountEdit.text().strip())))
        self.close()

    def set_list(self):
        self.ui.listNextTopics.clear()
        for t in self.next_topics:
            self.ui.listNextTopics.addItem(t.name)
        self.changed()

    def showEvent(self, event):
        self.set_list()

    def add_next_topic(self):
        topics = []
        for t in self.topics:
            n = next((x for x in self.next_topics if x.name == t.name), None)
            if n is None:
                topics.append(t)
        add_topic_dlg = NextTopicLayout(self, topics=topics, next_topics=self.next_topics)
        self.hide()
        add_topic_dlg.show()
        self.ui.deleteBtn.setEnabled(False)

    def delete_next_topic(self):
        if self.selected_row is not None:
            self.next_topics.pop(self.selected_row)
            self.set_list()
        self.ui.deleteBtn.setEnabled(False)

    def selected_row_changed(self, index):
        self.selected_row = index
        if self.selected_row is not None and self.selected_row >= 0:
            self.ui.deleteBtn.setEnabled(True)

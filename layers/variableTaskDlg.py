from PyQt5 import QtWidgets
from views.variableTaskDlg import Ui_Form


class VariableTaskLayout(QtWidgets.QDialog):
    def __init__(self, parent=None, tName='', pName='',  task_number='', desc='', variations=[]):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.topicEdit.setText(tName)
        self.ui.paragraphEdit.setText(pName)
        self.ui.numberEdit.setText(task_number)
        self.ui.descEdit.insertPlainText(desc)
        self.cancel_test = False

        for v in variations:
            self.ui.answerList.addItem(v)

        self.answer = ''

        self.ui.saveBtn.clicked.connect(self.save)
        self.ui.cancelBtn.clicked.connect(self.cancel)

        self.ui.answerList.currentRowChanged.connect(self.selected_row_changed)
        self.selected_row = None

    def cancel(self):
        self.cancel_test = True
        self.close()

    def selected_row_changed(self, index):
        self.selected_row = index
        if self.selected_row is not None and self.selected_row >= 0:
            self.ui.saveBtn.setEnabled(True)
        else:
            self.ui.saveBtn.setEnabled(False)

    def closeEvent(self, event):
        self.parent().show()

    def save(self):
        self.answer = str(self.ui.answerList.takeItem(self.selected_row).text())
        self.close()

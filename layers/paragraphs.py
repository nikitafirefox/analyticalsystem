from views.listForm import Ui_Form
from PyQt5 import QtWidgets
from layers.tasks import TasksLayer
from layers.paragraphSettings import ParagraphSettingsLayout


class ParagraphsLayer(QtWidgets.QDialog):
    def __init__(self, parent=None, paragraphs=[], topics=[], topic=''):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.setFixedHeight(250)
        self.setWindowTitle(topic.name + ': список тем')
        self.ui.extraBtn1.hide()
        self.ui.extraBtn2.hide()
        self.ui.extraBtn3.hide()
        self.next_topics = []
        for t in topics:
            if t != topic:
                self.next_topics.append(t)
        self.paragraphs = paragraphs
        self.ui.items.doubleClicked.connect(self.list_double_clicked)
        self.ui.items.currentRowChanged.connect(self.selected_row_changed)
        self.ui.deleteBtn.clicked.connect(self.delete_row)
        self.ui.addBtn.clicked.connect(self.add_paragraph)
        self.ui.editBtn.clicked.connect(self.edit_paragraph)
        self.selected_row = None

    def closeEvent(self, event):
        self.parent().show()

    def showEvent(self, event):
        self.set_list()

    def set_list(self):
        self.ui.items.clear()
        for p in self.paragraphs:
            self.ui.items.addItem(p.name)

    def list_double_clicked(self, event):
        dlg = TasksLayer(self, tasks=self.paragraphs[event.row()].tasks,
                                    paragraph_name=self.paragraphs[event.row()].name)
        self.hide()
        dlg.show()
        self.ui.deleteBtn.setEnabled(False)
        self.ui.editBtn.setEnabled(False)

    def selected_row_changed(self, index):
        self.selected_row = index
        if self.selected_row is not None and self.selected_row >= 0:
            self.ui.deleteBtn.setEnabled(True)
            self.ui.editBtn.setEnabled(True)

    def delete_row(self):
        if self.selected_row is not None:
            self.paragraphs.pop(self.selected_row)
            self.set_list()
        self.ui.deleteBtn.setEnabled(False)
        self.ui.editBtn.setEnabled(False)

    def edit_paragraph(self):
        if self.selected_row is not None:
            settings = ParagraphSettingsLayout(self, paragraphs=self.paragraphs,
                                                                      topics=self.next_topics,
                                                                      paragraph=self.paragraphs[self.selected_row])
            self.hide()
            settings.show()
            self.ui.deleteBtn.setEnabled(False)
            self.ui.editBtn.setEnabled(False)

    def add_paragraph(self):
        settings = ParagraphSettingsLayout(self, paragraphs=self.paragraphs,
                                                                  topics=self.next_topics)
        self.hide()
        settings.show()
        self.ui.deleteBtn.setEnabled(False)
        self.ui.editBtn.setEnabled(False)

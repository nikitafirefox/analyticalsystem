from PyQt5 import QtWidgets
from views.trueFalseTaskDlg import Ui_Form


class TrueFalseTaskLayout(QtWidgets.QDialog):
    def __init__(self, parent=None, tName='', pName='',  task_number='', desc=''):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.topicEdit.setText(tName)
        self.ui.paragraphEdit.setText(pName)
        self.ui.numberEdit.setText(task_number)
        self.ui.descEdit.insertPlainText(desc)
        self.cancel_test = False

        self.answer = ''

        self.ui.saveBtn.clicked.connect(self.save)
        self.ui.yesBtn.clicked.connect(self.clickedYes)
        self.ui.noBtn.clicked.connect(self.clickedNo)
        self.ui.cancelBtn.clicked.connect(self.cancel)

    def cancel(self):
        self.cancel_test = True
        self.close()

    def closeEvent(self, event):
        self.parent().show()

    def clickedNo(self):
        self.change('Нет')

    def clickedYes(self):
        self.change('Да')

    def change(self, a):
        self.answer = a
        self.ui.saveBtn.setEnabled(True)

    def save(self):
        self.close()
